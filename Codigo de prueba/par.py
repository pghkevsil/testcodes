import logging
from argparse import ArgumentParser

parser = ArgumentParser()

parser.add_argument("end", type=int, help="Fin del rango (incluio)")
# pares = [num for num in range(10) if num % 2 == 0]
args = parser.parse_args()
pares = [
    num for num in range(args.end + 1) if num % 2 == 0
]  # Añade +1 para incluir el 'end' en el rango

# Imprimir cada número par
for num in pares:
    print(f"{num}")
