#!/usr/bin/env python3
"""
Nombre del archivo: listar_numeros_pares.py
Descripción: Este script imprime todos los números pares hasta un número final especificado por el usuario a través de la línea de comandos.

Autor: Kevin Silva
Fecha: 05-02-2024
"""

import logging
from argparse import ArgumentParser


def listar_numeros_pares(end):
    """
    Genera e imprime una lista de números pares hasta el número especificado 'end'.

    Args:
    end (int): El número final del rango (incluido).
    """
    # Configuración del logging dentro de la función para evitar duplicados en caso de múltiples llamadas
    logging.basicConfig(
        level=logging.INFO, format="%(asctime)s - %(levelname)s - %(message)s"
    )

    pares = [num for num in range(end + 1) if num % 2 == 0]

    for num in pares:
        logging.info(f"Número par: {num}")


def main():
    """
    Función principal que parsea argumentos de la línea de comandos y llama a la función listar_numeros_pares.
    """
    parser = ArgumentParser(
        description="Listar números pares hasta un número final especificado."
    )
    parser.add_argument("end", type=int, help="Fin del rango (incluido)")
    args = parser.parse_args()

    listar_numeros_pares(args.end)


if __name__ == "__main__":
    main()
