from argparse import ArgumentParser

parser = ArgumentParser()

parser.add_argument("string", help="Cadena a repetir")
parser.add_argument(
    "-n", type=int, default=1, help="Cantidad de veces a repetir, por defecto 1"
)
args = parser.parse_args()

print(args.string * args.n)
