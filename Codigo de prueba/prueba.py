import logging
from argparse import ArgumentParser

# Configuración del logging
logging.basicConfig(
    level=logging.INFO, format="%(asctime)s - %(levelname)s - %(message)s"
)
logging.basicConfig(
    level=logging.WARNING, format="%(asctime)s - %(levelname)s - %(message)s"
)

# Crear el analizador de argumentos
parser = ArgumentParser()

# Añadir argumento posicional "end" con explicación
parser.add_argument("end", type=int, help="Fin del rango (incluido)")

# Analizar los argumentos de la línea de comandos
args = parser.parse_args()

# Generar lista de números pares hasta el rango especificado por 'end'
pares = [
    num for num in range(args.end + 1) if num % 2 == 0
]  # Añade +1 para incluir el 'end' en el rango

# Registrar cada número par usando logging en lugar de print
for num in pares:
    logging.info(f"Número par: {num}")
