"""
Nombre del archivo: rango_numeros.py
Descripción: Este script imprime todos los números en un rango especificado por el usuario.
             Acepta dos argumentos enteros desde la línea de comandos: el inicio y el fin del rango,
             e imprime cada número en este rango, incluyendo ambos extremos.

Autor: Kevin Silva
Fecha: 05-02-2024
Versión: 1.0
"""

from argparse import ArgumentParser


def print_range(start, end):
    """
    Imprime todos los números en el rango especificado, incluyendo ambos extremos.

    Args:
    start (int): El número inicial del rango.
    end (int): El número final del rango.
    """
    for i in range(start, end + 1):
        print(f"{i}")


def main():
    """
    Función principal que parsea argumentos de la línea de comandos y llama a la función print_range.
    """
    parser = ArgumentParser(
        description="Imprime todos los números en un rango especificado."
    )  # Instanciar la clase con un docstring

    parser.add_argument("start", type=int, help="Inicio del rango (incluido)")
    parser.add_argument("end", type=int, help="Fin del rango (incluido)")

    args = parser.parse_args()

    print_range(args.start, args.end)


if __name__ == "__main__":
    main()
